##!/usr/bin/env python
# -*- coding: utf-8 -*-

# Import libs
#from __future__ import absolute_import
#from __future__ import division
#from __future__ import print_function
#from __future__ import unicode_literals

try:
    import ConfigParser as configparser
except ImportError:
    import configparser

import socket
import sys
import re
import argparse

from datetime import datetime
from pprint import pformat
# from collections import OrderedDict

 
    # Define functions    
if sys.version_info[0] == 2:
    reload(sys)
    # sys.setdefaultencoding('utf-8')


def warning(*objs):
    print("WARNING: ", *objs, file=sys.stderr)


def debug(*objs):
    print("DEBUG:\n", *objs, file=sys.stderr)


def get_date(date_string, uts=False):
    if not uts:
        return datetime.strptime(date_string, "%a %b %d %H:%M:%S %Y")
    else:
        return datetime.fromtimestamp(float(date_string))

class ConfigLoader(object):

    def __init__(self, config_file):

        self.settings = {}
        #self. = OrderedDict()
        config = configparser.RawConfigParser()
        contents = config.read(config_file)

        if not contents:
            print('Config file does not exist or is unreadable, using defaults')
            self.load_default_settings()

        for section in config.sections():
            if section == 'ovpnManager':
                self.parse_global_section(config)
            else:
                print('Config file has missing section header or invalid format, using defaults.')
                self.load_default_settings()

    def load_default_settings(self):
        warning('Using default settings => /var/run/openvpn.socket')
        # self.settings = {'ovpnServer': 'Default Server'}
        self.settings = {'host': '/var/run/openvpn.socket',
                         'port': '7505', 'password': ''}

    def parse_global_section(self, config):
        global_vars = ['host', 'port', 'password' ]
        for var in global_vars:
            try:
                self.settings[var] = config.get('ovpnManager', var)
            except configparser.NoOptionError:
                pass
        if args.debug:
            debug("=== begin section\n{0!s}\n=== end section".format(self.settings))


class ovpnManager(object):

    def __init__(self, ovpn):
        self.ovpn = ovpn
        self._socket_connect(ovpn)
        if self.s:
            self.collect_data(ovpn)
            self._socket_disconnect()

    def collect_data(self, ovpn):
        version = self.send_command('version\n')
        ovpn['version'] = self.parse_version(version)
        state = self.send_command('state\n')
        ovpn['state'] = self.parse_state(state)
        stats = self.send_command('load-stats\n')
        ovpn['stats'] = self.parse_stats(stats)
        status = self.send_command('status 3\n')
        ovpn['sessions'] = self.parse_status(status)

    def _socket_send(self, command):
        if sys.version_info[0] == 2:
            self.s.send(command)
        else:
            self.s.send(bytes(command, 'utf-8'))

    def _socket_recv(self, length):
        if sys.version_info[0] == 2:
            return self.s.recv(length)
        else:
            return self.s.recv(length).decode('utf-8')

    def _socket_connect(self, ovpn):
        host = ovpn['host']
        port = int(ovpn['port'])
        timeout = 3
        if re.match( '^/.*', host):
            try:
                s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
                s.settimeout(timeout)
                self.s = s.connect(host)
                ovpn['socket_connected'] = True
            except socket.error:
                self.s = False
                ovpn['socket_connected'] = False
        else:
            try:
                self.s = socket.create_connection((host, port), timeout)
                ovpn['socket_connected'] = True
            except socket.error:
                self.s = False
                ovpn['socket_connected'] = False

    def _socket_disconnect(self):
        self._socket_send('quit\n')
        self.s.close()

    def send_command(self, command):
        self._socket_send(command)
        data = ''
        while 1:
            socket_data = self._socket_recv(1024)
            socket_data = re.sub('>INFO(.)*\r\n', '', socket_data)
            data += socket_data
            if command == 'load-stats\n' and data != '':
                break
            elif data.endswith("\nEND\r\n"):
                break
        if args.debug:
            debug("=== begin raw data\n{0!s}\n=== end raw data".format(data))
        return data


#main function
def main():
    cfg = ConfigLoader(args.config)
    monitor = ovpnManager(cfg.settings)
    if args.debug:
        pretty_ovpn = pformat((dict(monitor.ovpn)))
        debug("=== begin ovpn\n{0!s}\n=== end ovpn".format(pretty_ovpn))


def collect_args():
    parser = argparse.ArgumentParser(
        description='Display a html page with openvpn status and connections')
    parser.add_argument('-d', '--debug', action='store_true',
                        required=False, default=False,
                        help='Run in debug mode')
    parser.add_argument('-c', '--config', type=str,
                        required=False, default='./openvpn-monitor.cfg',
                        help='Path to config file openvpn.cfg')
    parser.add_argument('-h', '--host', type=str,
                        required=False, default='/var/run/openvpn.socket',
                        help='Name, IP address or path to socket for OVPN server to be managed')
    parser.add_argument('-p', '--port', type=str,
                        required=False, default='7505',
                        help='Port number of OVPN server to be managed (igonred if unix socket)')
    parser.add_argument('-P', '--password', type=str,
                        required=False, default='None',
                        help='Password for OVPN server management interface')
    return parser

if __name__ == "__main__":
    args = collect_args().parse_args()
    main() 
             
            