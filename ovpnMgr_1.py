#!/usr/bin/python
# telnet program example
import socket, select, string, sys, re, time
# from plistlib import Data

global msgRx
global msgTx
global msgUsr
# global ioTimout

ioTimeout = 2

msgRx = None
msgTx = None
msgUsr = None
 
#main function
if __name__ == "__main__":
    
    # Define functions    
    def rx():
        global msgRx
        global msgUsr
        
        socket_list = [sys.stdin, s]
         
        # Get the list sockets which are readable
        read_sockets, write_sockets, error_sockets = select.select(socket_list , [], [])
        if read_sockets:
            for sock in read_sockets:
                #incoming message from remote server
                if sock == s:
                    data = s.recv(4096)
                    if not data :
                        print 'Connection closed'
                        sys.exit()
                    else :
                        #print data
                        #sys.stdout.write(data)
                        msgRx = data
                #user entered a message
                else :
                    msgUsr = sys.stdin.readline()
                    #s.send(msgUsr)

    def tx():
        global msgRx
        global msgTx
        global msgUsr
        
        if msgUsr :
            s.send(msgUsr)
            msgUsr = None
        if msgRx :
            sys.stdout.write(msgRx)
            msgRx = None
        if msgTx :
            s.send(msgTx)
            #sys.stdout.write(msgTx)
            msgTx = None
    
    def xceive():
        tx()
        time.sleep(0.2)
        rx()
    
    # Test command line args and connect to appropriate service    
    if(len(sys.argv) == 3) :
        s = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        # s.settimeout(2)
        s.settimeout(ioTimeout)
        host = sys.argv[1]
        password = sys.argv[2]
        try :
            s.connect(host)
        except :
            print 'Unable to connect'
            sys.exit()
    elif(len(sys.argv) >= 4) :
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # s.settimeout(2)
        s.settimeout(ioTimeout)
        host = sys.argv[1]
        port = int(sys.argv[2])
        password = sys.argv[3]
        try :
            s.connect((host, port))
        except :
            print 'Unable to connect'
            sys.exit()
    else:
        print 'Usage : ovpnMgr.py hostname port password'
        print '     or ovpnMgr.py path_to_socket password'
        sys.exit()

    print 'Connected to remote host'
             
    # Log in
               
    time.sleep(0.25)
    
    authString = password + "\n"
    print 'Sending password ...'
    #s.send("\n")
    msgTx = authString
    tx()
    time.sleep(0.2)
    rx()
    if msgRx :
        if re.match('.*SUCCESS: password is correct.*', msgRx) is not None: # or re.match('^SUCCESS:.*', msgRx) is not None:
            authState = True
            msgRx = None
            print "Login OK."
            time.sleep(0.25)
        else:
            time.sleep(0.2)
            
    while 1:
        msgTx = "hold\n"
        xceive()
              
        tx()
        
        time.sleep(0.001) 
             
            